// My Homepage widget
import 'package:firebase_auth/firebase_auth.dart';
import 'package:first_flutter_app/database.dart';
import 'package:first_flutter_app/myHomePage.dart';
import 'package:flutter/material.dart';

import 'Post.dart';
import 'postList.dart';
import 'textInputWidget.dart';

class MyHomePage extends StatefulWidget {
  final FirebaseUser user;

  MyHomePage(this.user);

  @override
  State<MyHomePage> createState() => _MyHomePageState();
}

class _MyHomePageState extends State<MyHomePage> {
  List<Post> posts = [];

  void newPost(String text) {
    var post = new Post(text, widget.user.displayName);
    post.setID(savePost(post));
    this.setState(() {
      posts.add(post);
    });
  }

  void updatePosts() {
    getAllPosts().then((posts) => {
      this.setState(() {
        this.posts = posts;
      })
    });
  }

  @override
  void initState(){
    super.initState();
    updatePosts();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        appBar: AppBar(title: Text('First App')),
        body: Column(
          children: <Widget>[
            Expanded(child: PostList(this.posts, widget.user)),
            TextInputWidget(this.newPost)
          ],
        ));
  }
}
