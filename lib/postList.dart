// PostList widget
import 'package:firebase_auth/firebase_auth.dart';
import 'package:flutter/material.dart';

import 'package:first_flutter_app/Post.dart';

class PostList extends StatefulWidget {
  final List<Post> listItems;
  final FirebaseUser user;

  PostList(this.listItems, this.user);

  @override
  State<PostList> createState() => _PostListState();
}

class _PostListState extends State<PostList> {
  void like(Function callback) {
    this.setState(() {
      callback();
    });
  }

  @override
  Widget build(BuildContext context) {
    return ListView.builder(
      itemCount: this.widget.listItems.length,
      itemBuilder: (context, index) {
        var post = this.widget.listItems[index];
        return Card(
            child: Row(children: <Widget>[
          Expanded(
              child: ListTile(
                  title: Text(post.body), subtitle: Text(post.author))),
          Row(children: <Widget>[
            Container(
                child: Text(post.usersLiked.length.toString(),
                    style: TextStyle(fontSize: 20)),
                padding: EdgeInsets.fromLTRB(0, 0, 10, 0)),
            IconButton(
              onPressed: () => this.like(() => post.likePost(widget.user)),
              icon: Icon(Icons.thumb_up),
              color: post.usersLiked.contains(widget.user.uid)
                  ? Colors.green
                  : Colors.black,
            )
          ]),
        ]));
      },
    );
  }
}
