import 'package:firebase_database/firebase_database.dart';
import 'Post.dart';

final databaseReference = FirebaseDatabase.instance.reference();

DatabaseReference savePost(Post post) {
  var id = databaseReference.child('posts/').push();
  id.set(post.toJSON());
  return id;
}

void updatePost(Post post, DatabaseReference id){
  id.update(post.toJSON());
}

Future<List<Post>> getAllPosts() async {
  DataSnapshot dataSnapshot = await databaseReference.child('posts/').once();
  List<Post> posts = [];
  if (dataSnapshot.value != null) {
    dataSnapshot.value.forEach((key, value) {
      Post post = createPost(value);
      post.setID(databaseReference.child('posts/' + key));
      posts.add(post);
    });
  }
  return posts;
}